package com.cluebix.payal.lafood.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cluebix.payal.lafood.R;
import com.cluebix.payal.lafood.adapters.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends Fragment {

    ViewPagerAdapter adapter;
    ViewPager slider_viewPager;
    LinearLayout llPagerDots;
    Toolbar toolbar;
    ViewPager tabs_viewPager;
    TabLayout tabLayout;
    public int []sale_image_array={R.drawable.dummy2,R.drawable.dummy2,R.drawable.dummy2};
    AppBarLayout appBarLayout;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        findViews(view);
        setUpSliderImageHeight();


        init();
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 2000, 5000);


        setupViewPager(tabs_viewPager);
        tabLayout.setupWithViewPager(tabs_viewPager);
        return view;
    }










    public void findViews(View view) {
        slider_viewPager=(ViewPager)view.findViewById(R.id.home_slider_viewPager);
        llPagerDots = (LinearLayout) view.findViewById(R.id.home_frag_linear_dots);
        toolbar=(Toolbar) view.findViewById(R.id.home_frag_toolbar);
        tabs_viewPager=(ViewPager)view.findViewById(R.id.home_tabs_viewpager);
        tabLayout=(TabLayout)view.findViewById(R.id.home_frag_tabLayout);
        appBarLayout=(AppBarLayout)view.findViewById(R.id.home_frag_appBarLayout);
    }

    public void setUpSliderImageHeight() {
        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams)appBarLayout.getLayoutParams();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float heightPx = (displayMetrics.widthPixels) *  2/3 ;
        float extraHeightPx = (int)getResources().getInteger(R.integer.custom_tab_layout_height_for_java_code) * displayMetrics.density;
        heightPx = heightPx + extraHeightPx;
        lp.height = (int)heightPx;
    }








    public void init() {
        adapter=new ViewPagerAdapter(getContext(),sale_image_array);
        slider_viewPager.setAdapter(adapter);

        addBottomDots(0);
        slider_viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }
    public void addBottomDots(int currentPage) {
        TextView[] dots = new TextView[sale_image_array.length];
        llPagerDots.removeAllViews();

        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(getContext());
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(Color.parseColor("#000000"));
            llPagerDots.addView(dots[i]);
        }
        if (dots.length > 0)
            dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
    }
    public class SliderTimer extends TimerTask {
        @Override
        public void run() {
            if (getActivity()!=null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (slider_viewPager.getCurrentItem() < sale_image_array.length - 1) {
                            slider_viewPager.setCurrentItem(slider_viewPager.getCurrentItem() + 1);
                        } else {
                            slider_viewPager.setCurrentItem(0);
                        }
                    }
                });
            }
    }









    public void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter2 adapter = new ViewPagerAdapter2(getChildFragmentManager());
        adapter.addFragment(new HomeSpecialFragment(), "Specials");
        adapter.addFragment(new HomeMenuFragment(), "Menu");
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter2 extends FragmentPagerAdapter {
        public final List<Fragment> mFragmentList = new ArrayList<>();
        public final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter2(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }






}
