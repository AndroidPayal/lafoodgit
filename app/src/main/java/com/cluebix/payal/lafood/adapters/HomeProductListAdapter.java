package com.cluebix.payal.lafood.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.cluebix.payal.lafood.R;
import com.cluebix.payal.lafood.datas.HomeProductListItem;

import java.util.ArrayList;

public class HomeProductListAdapter extends RecyclerView.Adapter<HomeProductListAdapter.ViewHolder> {


    private Context context;
    private ArrayList<HomeProductListItem> listItems;

    public HomeProductListAdapter(Context context, ArrayList listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_home_productlist,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

         public ViewHolder(final View itemView) {
            super(itemView);

        }
    }


}
