package com.cluebix.payal.lafood.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cluebix.payal.lafood.R;
import com.cluebix.payal.lafood.adapters.HomeFeaturedAdapter;
import com.cluebix.payal.lafood.adapters.MenuCategoryGridAdapter;
import com.cluebix.payal.lafood.datas.HomeFeaturedItem;
import com.cluebix.payal.lafood.datas.MenuCategoryItem;

import java.util.ArrayList;

public class HomeMenuFragment extends Fragment {

    RecyclerView recyclerView_category;
    TextView text_category;
    ArrayList<MenuCategoryItem> menuCategoryList = new ArrayList<>();
    MenuCategoryGridAdapter menuCategoryGridAdapter;

    public HomeMenuFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_home_menu, container, false);

        findViews(view);
        getCategoryList();
        setCategoryRecyclerAdapter();

        return view;
    }

    private void setCategoryRecyclerAdapter() {
        LinearLayoutManager layoutManager = new GridLayoutManager(getContext(), calculateNoOfColumns(getContext()));
        menuCategoryGridAdapter = new MenuCategoryGridAdapter(getContext(), menuCategoryList);
        recyclerView_category.setLayoutManager(layoutManager);
        recyclerView_category.setAdapter(menuCategoryGridAdapter);
    }

    private void getCategoryList() {
        menuCategoryList.add(new MenuCategoryItem("1"));
        menuCategoryList.add(new MenuCategoryItem("1"));
        menuCategoryList.add(new MenuCategoryItem("1"));
        menuCategoryList.add(new MenuCategoryItem("1"));
        menuCategoryList.add(new MenuCategoryItem("1"));
        menuCategoryList.add(new MenuCategoryItem("1"));
        menuCategoryList.add(new MenuCategoryItem("1"));
        menuCategoryList.add(new MenuCategoryItem("1"));
        menuCategoryList.add(new MenuCategoryItem("1"));
        menuCategoryList.add(new MenuCategoryItem("1"));
    }

    private void findViews(View view) {
        recyclerView_category = view.findViewById(R.id.common_recyclerView);
        text_category=view.findViewById(R.id.common_textview);
        text_category.setText(getResources().getString(R.string.allCategory));
    }

    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        return (int) (dpWidth / 180);
    }

}
