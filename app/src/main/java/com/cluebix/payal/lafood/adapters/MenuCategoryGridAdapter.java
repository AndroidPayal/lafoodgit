package com.cluebix.payal.lafood.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.cluebix.payal.lafood.ProductListActivity;
import com.cluebix.payal.lafood.R;
import com.cluebix.payal.lafood.datas.MenuCategoryItem;
import com.cluebix.payal.lafood.datas.MenuCategoryItem;

import java.util.ArrayList;

public class MenuCategoryGridAdapter extends RecyclerView.Adapter<MenuCategoryGridAdapter.ViewHolder> {


    private Context context;
    private ArrayList<MenuCategoryItem> listItems;

    public MenuCategoryGridAdapter(Context context, ArrayList listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_grid_menu_category,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MenuCategoryItem item = listItems.get(position);
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        FrameLayout frameLayout;
         public ViewHolder(final View itemView) {
            super(itemView);
            frameLayout=itemView.findViewById(R.id.frame_container);
            frameLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(context, ProductListActivity.class);
                    context.startActivity(i);
                }
            });
         }
    }


}
