package com.cluebix.payal.lafood.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.cluebix.payal.lafood.ProductListActivity;
import com.cluebix.payal.lafood.R;
import com.cluebix.payal.lafood.datas.HomeFeaturedItem;

import java.util.ArrayList;

public class HomeFeaturedAdapter extends RecyclerView.Adapter<HomeFeaturedAdapter.ViewHolder> {


    private Context context;
    private ArrayList<HomeFeaturedItem> listItems;

    public HomeFeaturedAdapter(Context context, ArrayList listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_home_featured,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        HomeFeaturedItem item = listItems.get(position);
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayout;

         public ViewHolder(final View itemView) {
            super(itemView);
             linearLayout = itemView.findViewById(R.id.adapter_featured_linear_parent);
             linearLayout.setLayoutParams(new LinearLayout.LayoutParams(900, ViewGroup.LayoutParams.WRAP_CONTENT));//900 px
             // set it to 3/4th of screen width

             linearLayout.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent i=new Intent(context, ProductListActivity.class);
                     context.startActivity(i);
                 }
             });
        }
    }


}
