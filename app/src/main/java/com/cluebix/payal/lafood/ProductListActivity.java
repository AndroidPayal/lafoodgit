package com.cluebix.payal.lafood;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.cluebix.payal.lafood.adapters.HomeProductListAdapter;
import com.cluebix.payal.lafood.adapters.MenuCategoryGridAdapter;
import com.cluebix.payal.lafood.datas.HomeProductListItem;
import com.cluebix.payal.lafood.datas.MenuCategoryItem;

import java.util.ArrayList;

public class ProductListActivity extends AppCompatActivity {


    Toolbar toolbar_category_name;
    ImageButton product_back;
    ImageView imageView_toolbar;
    TextView text_heading_recycler;
    RecyclerView recyclerView;
    ArrayList<HomeProductListItem> arrayProductList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
          super.onCreate(savedInstanceState);
      /*    requestWindowFeature(Window.FEATURE_NO_TITLE);
          getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
     */     setContentView(R.layout.activity_product_list);

        findViews();
        getProductList();
        setRecyclerAdapter();

    }

    private void getProductList() {
        arrayProductList.add(new HomeProductListItem("1"));
        arrayProductList.add(new HomeProductListItem("1"));
        arrayProductList.add(new HomeProductListItem("1"));
        arrayProductList.add(new HomeProductListItem("1"));
        arrayProductList.add(new HomeProductListItem("1"));
        arrayProductList.add(new HomeProductListItem("1"));
        arrayProductList.add(new HomeProductListItem("1"));
    }

    private void findViews() {
        toolbar_category_name= findViewById(R.id.product_toolbar);
        product_back=findViewById(R.id.product_back);
        imageView_toolbar=findViewById(R.id.imageView_slider);
        text_heading_recycler=findViewById(R.id.common_textview);
        recyclerView =findViewById(R.id.common_recyclerView);

        additionalScreenDesigning();
    }

    private void additionalScreenDesigning() {
        imageView_toolbar.setImageDrawable(getResources().getDrawable(R.drawable.dummy2));
        text_heading_recycler.setText(getResources().getString(R.string.productName));
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(),
                DividerItemDecoration.VERTICAL));/*To add divider in product list*/
    }

    public void setRecyclerAdapter(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        HomeProductListAdapter homeProductListAdapter = new HomeProductListAdapter(getApplicationContext(), arrayProductList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(homeProductListAdapter);
    }


}
