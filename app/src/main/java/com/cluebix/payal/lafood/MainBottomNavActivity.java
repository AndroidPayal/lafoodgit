package com.cluebix.payal.lafood;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.cluebix.payal.lafood.fragments.CartFragment;
import com.cluebix.payal.lafood.fragments.HomeFragment;
import com.cluebix.payal.lafood.fragments.ProfileFragment;
import com.cluebix.payal.lafood.helpers.BottomNavigationBehavior;

public class MainBottomNavActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView navigationView;

    //this is main activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main_bottom_nav);

        navigationView = (BottomNavigationView) findViewById(R.id.main_bottom_navigationView);
        navigationView.setOnNavigationItemSelectedListener(this);


      /*  CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigationView.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationBehavior());
*/
        loadFragment(new HomeFragment());

    }



    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.navigation_home:
                loadFragment(new HomeFragment());
                break;
            case R.id.navigation_cart:
                loadFragment(new CartFragment());
                break;
            case R.id.navigation_profile:
                loadFragment(new ProfileFragment());
                break;
        }
        return false;
    }



    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_bottom_nav_frame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
