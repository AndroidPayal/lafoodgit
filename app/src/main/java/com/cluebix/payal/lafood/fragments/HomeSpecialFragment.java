package com.cluebix.payal.lafood.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cluebix.payal.lafood.adapters.HomeFeaturedAdapter;
import com.cluebix.payal.lafood.adapters.HomeProductListAdapter;
import com.cluebix.payal.lafood.R;
import com.cluebix.payal.lafood.datas.HomeFeaturedItem;
import com.cluebix.payal.lafood.datas.HomeProductListItem;

import java.util.ArrayList;


public class HomeSpecialFragment extends Fragment {

    RecyclerView recyclerView_featured,recyclerView_recommended;
    LinearLayout linearLayout_featured,linearLayout_recommended;
    TextView textView_featured,textView_recommended;
    HomeFeaturedAdapter homeFeaturedAdapter;
    HomeProductListAdapter homeRecommendedAdapter;
    ArrayList<HomeFeaturedItem> arrayFeaturedItems = new ArrayList<>();
    ArrayList<HomeProductListItem> arrayRecommendedItems = new ArrayList<>();
    LinearLayoutManager layoutManager;

    public HomeSpecialFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_home_special, container, false);

        findViews(view);

        getFeaturedItemList();
        getRecommendedItemList();

        setFeaturedRecyclerViewAdapter();
        setRecommendedRecyclerViewAdapter();

        return view;
    }



    private void findViews(View view) {
        linearLayout_featured =(LinearLayout)view.findViewById(R.id.featured_list);
        recyclerView_featured= linearLayout_featured.findViewById(R.id.common_recyclerView);
        textView_featured =linearLayout_featured.findViewById(R.id.common_textview);
        textView_featured.setText(getResources().getString(R.string.featured));

        linearLayout_recommended=(LinearLayout)view.findViewById(R.id.recommendedlist);
        recyclerView_recommended=linearLayout_recommended.findViewById(R.id.common_recyclerView);
        textView_recommended=linearLayout_recommended.findViewById(R.id.common_textview);
        textView_recommended.setText(getResources().getString(R.string.recommended));
    }




    private void getRecommendedItemList() {
        arrayRecommendedItems.add(new HomeProductListItem("1"));
        arrayRecommendedItems.add(new HomeProductListItem("1"));
        arrayRecommendedItems.add(new HomeProductListItem("1"));
        arrayRecommendedItems.add(new HomeProductListItem("1"));
    }
    private void getFeaturedItemList() {
        arrayFeaturedItems.add(new HomeFeaturedItem("1"));
        arrayFeaturedItems.add(new HomeFeaturedItem("1"));
        arrayFeaturedItems.add(new HomeFeaturedItem("1"));
        arrayFeaturedItems.add(new HomeFeaturedItem("1"));
    }




    private void setRecommendedRecyclerViewAdapter() {
        layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        homeRecommendedAdapter = new HomeProductListAdapter(getContext(), arrayRecommendedItems);
        recyclerView_recommended.setLayoutManager(layoutManager);
        recyclerView_recommended.setAdapter(homeRecommendedAdapter);
        recyclerView_recommended.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));/*To add divider in product list*/
    }

    private void setFeaturedRecyclerViewAdapter() {
        layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        homeFeaturedAdapter = new HomeFeaturedAdapter(getContext(), arrayFeaturedItems);
        recyclerView_featured.setLayoutManager(layoutManager);
        recyclerView_featured.setAdapter(homeFeaturedAdapter);
    }




}
