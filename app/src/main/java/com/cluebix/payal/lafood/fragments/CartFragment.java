package com.cluebix.payal.lafood.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cluebix.payal.lafood.R;
import com.cluebix.payal.lafood.adapters.HomeProductListAdapter;
import com.cluebix.payal.lafood.datas.HomeProductListItem;

import java.util.ArrayList;

public class CartFragment extends Fragment {

    Toolbar myToolbar;
    RecyclerView recyclerView_recommended;
    ArrayList<HomeProductListItem> arrayRecommendedItems = new ArrayList<>();
    TextView textView_recommended,text_toolbar_name;
    LinearLayoutManager layoutManager;
    HomeProductListAdapter homeProductListAdapter;

    public CartFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart, container, false);

        findViews(view);
        getRecommendedItemList();

        setRecommendedRecyclerViewAdapter();
        return view;
    }

    private void findViews(View view) {
        myToolbar = (Toolbar)view.findViewById(R.id.home_frag_toolbar);
        recyclerView_recommended=view.findViewById(R.id.common_recyclerView);
        textView_recommended=view.findViewById(R.id.common_textview);
        text_toolbar_name=view.findViewById(R.id.text_toolbar_name);


      //  textView_recommended.setVisibility(View.GONE);
        textView_recommended.setText("Added Items");
        text_toolbar_name.setText(getResources().getString(R.string.cartScreen)+"0");
    }

    private void getRecommendedItemList() {
        arrayRecommendedItems.add(new HomeProductListItem("1"));
        arrayRecommendedItems.add(new HomeProductListItem("1"));
        arrayRecommendedItems.add(new HomeProductListItem("1"));
        arrayRecommendedItems.add(new HomeProductListItem("1"));
        arrayRecommendedItems.add(new HomeProductListItem("1"));
        arrayRecommendedItems.add(new HomeProductListItem("1"));
        arrayRecommendedItems.add(new HomeProductListItem("1"));
    }

    private void setRecommendedRecyclerViewAdapter() {
        layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        homeProductListAdapter = new HomeProductListAdapter(getContext(), arrayRecommendedItems);
        recyclerView_recommended.setLayoutManager(layoutManager);
        recyclerView_recommended.setAdapter(homeProductListAdapter);
        recyclerView_recommended.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));/*To add divider in product list*/
    }

}
